package com.cauliflower.danielt.retrofitsample.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.TextView;
import butterknife.BindView;
import butterknife.ButterKnife;
import com.cauliflower.danielt.retrofitsample.R;
import com.cauliflower.danielt.retrofitsample.RetrofitSampleApp;
import com.cauliflower.danielt.retrofitsample.network.TmdbApiService;
import com.cauliflower.danielt.retrofitsample.tmdb.BriefMovie;
import com.cauliflower.danielt.retrofitsample.tmdb.DetailedMovie;
import com.cauliflower.danielt.retrofitsample.viewmodel.MainViewModel;
import com.cauliflower.danielt.retrofitsample.viewmodel.ViewModelFactory;

import javax.inject.Inject;


public class MainActivity extends AppCompatActivity {

    private static final String TAG = MainActivity.class.getSimpleName();
    @BindView(R.id.tv_movieInfo)
    TextView tvMovieInfo;
    private MainViewModel mViewModel;
    @Inject
    TmdbApiService tmdbApiService;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        ((RetrofitSampleApp) getApplication()).getAppComponent().inject(this);

        getViewModel();
        subscribeObserver();
        mViewModel.searchMovieById("76341");
    }

    private void getViewModel() {
        ViewModelFactory factory = new ViewModelFactory(getApplication());
        mViewModel = ViewModelProviders.of(this, factory).get(MainViewModel.class);
    }

    private void subscribeObserver() {
        mViewModel.getSearchedMovie().observe(this, movie -> {
            if (movie == null) {
                return;
            }
            logMovieInfo(movie);
        });

        mViewModel.getSearchedMovies().observe(this, movies -> {
            if (movies == null) {
                return;
            }

            for (BriefMovie movie : movies) {
                Log.i(TAG, "msg from observer:" + movie.getId() + "," + movie.getOriginal_title());
            }
        });
    }

    private void logMovieInfo(DetailedMovie movie) {
        String message = "OriginalTitle:" + movie.getOriginal_title();
        tvMovieInfo.setText(message);
        Log.i(TAG, message);
    }

}
