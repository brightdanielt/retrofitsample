package com.cauliflower.danielt.retrofitsample.tmdb;

import java.util.List;

public class Actor {
    /**
     * birthday : 1958-05-25
     * known_for_department : Acting
     * deathday : null
     * id : 3543
     * name : Jamie Foreman
     * also_known_as : []
     * gender : 2
     * biography : From Wikipedia, the free encyclopedia

     Jamie Foreman (born 1958) is an English actor best known for his roles as Duke in Layer Cake (2004) and Bill Sikes in Roman Polanski's Oliver Twist (2005). He played opposite Ray Winstone and Kathy Burke in Gary Oldman's Nil by Mouth and also featured in Elizabeth, Gangster No. 1 and Sleepy Hollow. He appeared in the 2006 Doctor Who episode "The Idiot's Lantern". He also featured as a racist taxi driver in The Football Factory. Foreman also played Basta in the film Inkheart. He also appeared in one episode of Law and Order: UK.

     He is the son of Freddie Foreman, a former East End gangster and associate of the Kray twins.

     His recent work for BBC Radio includes the title role in Wes Bell, directed by Matthew Broughton, and the six part series Hazelbeach by David Stafford and Caroline Stafford. He also played a small role in I'll Sleep When I'm Dead.

     He is a fan of Tottenham Hotspur.

     Description above from the Wikipedia article Jamie Foreman, licensed under CC-BY-SA, full list of contributors on Wikipedia.
     * popularity : 1.623
     * place_of_birth : Bermondsey, South London
     * profile_path : /yKUHNdA0Oe9JJqHJDLUMRx9Bnfa.jpg
     * adult : false
     * imdb_id : nm0286044
     * homepage : null
     */

    private String birthday;
    private String known_for_department;
    private Object deathday;
    private int id;
    private String name;
    private int gender;
    private String biography;
    private double popularity;
    private String place_of_birth;
    private String profile_path;
    private boolean adult;
    private String imdb_id;
    private Object homepage;
    private List<?> also_known_as;

    public String getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = birthday;
    }

    public String getKnown_for_department() {
        return known_for_department;
    }

    public void setKnown_for_department(String known_for_department) {
        this.known_for_department = known_for_department;
    }

    public Object getDeathday() {
        return deathday;
    }

    public void setDeathday(Object deathday) {
        this.deathday = deathday;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getGender() {
        return gender;
    }

    public void setGender(int gender) {
        this.gender = gender;
    }

    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }

    public double getPopularity() {
        return popularity;
    }

    public void setPopularity(double popularity) {
        this.popularity = popularity;
    }

    public String getPlace_of_birth() {
        return place_of_birth;
    }

    public void setPlace_of_birth(String place_of_birth) {
        this.place_of_birth = place_of_birth;
    }

    public String getProfile_path() {
        return profile_path;
    }

    public void setProfile_path(String profile_path) {
        this.profile_path = profile_path;
    }

    public boolean isAdult() {
        return adult;
    }

    public void setAdult(boolean adult) {
        this.adult = adult;
    }

    public String getImdb_id() {
        return imdb_id;
    }

    public void setImdb_id(String imdb_id) {
        this.imdb_id = imdb_id;
    }

    public Object getHomepage() {
        return homepage;
    }

    public void setHomepage(Object homepage) {
        this.homepage = homepage;
    }

    public List<?> getAlso_known_as() {
        return also_known_as;
    }

    public void setAlso_known_as(List<?> also_known_as) {
        this.also_known_as = also_known_as;
    }
}
