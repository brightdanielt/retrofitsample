package com.cauliflower.danielt.retrofitsample.tmdb;

public class DatesBean {
    /**
     * maximum : 2019-03-22
     * minimum : 2019-02-02
     */

    private String maximum;
    private String minimum;

    public String getMaximum() {
        return maximum;
    }

    public void setMaximum(String maximum) {
        this.maximum = maximum;
    }

    public String getMinimum() {
        return minimum;
    }

    public void setMinimum(String minimum) {
        this.minimum = minimum;
    }
}
