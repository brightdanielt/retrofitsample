package com.cauliflower.danielt.retrofitsample;

import android.app.Application;
import com.squareup.leakcanary.LeakCanary;

public class RetrofitSampleApp extends Application {

    private AppComponent appComponent;

    public RetrofitSampleApp() {
    }

    @Override
    public void onCreate() {
        super.onCreate();
        //LeakCanary help me find out memory leak
        if (!LeakCanary.isInAnalyzerProcess(this)) {
            LeakCanary.install(this);
        }
        appComponent = DaggerAppComponent.create();
    }

    public AppComponent getAppComponent() {
        return appComponent;
    }
}
