package com.cauliflower.danielt.retrofitsample.network;

import com.cauliflower.danielt.retrofitsample.tmdb.Actor;
import com.cauliflower.danielt.retrofitsample.tmdb.Credits;
import com.cauliflower.danielt.retrofitsample.tmdb.DetailedMovie;
import com.cauliflower.danielt.retrofitsample.tmdb.MovieResponse;
import io.reactivex.Observable;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;


public interface TmdbApiService {

    /**
     * Info of query
     *  language:
     * Pass a ISO 639-1 value to display translated data for the fields that support it.
     *
     * minLength: 2
     * pattern: ([a-z]{2})-([A-Z]{2})
     * default: en-US
     * */

    /**
     * Sample url for searching movie detail
     * <p>
     * https://api.themoviedb.org/3/
     * movie/
     * 76341
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     */
    @GET("movie/{movieId}")
    Observable<DetailedMovie> getMovieDetail(
            @Path("movieId") String movieId);

    /**
     * Sample url for searching movies by title
     * <p>
     * https://api.themoviedb.org/3/
     * search/
     * movie
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &query=IRON
     * &page=1
     * &include_adult=false
     */
    @GET("search/movie")
    Call<MovieResponse> getMoviesByTitle(
            @Query("query") String movieTitle
    );

    /**
     * https://api.themoviedb.org/3/
     * movie/
     * upcoming
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &page=1
     * &region=tw
     */
    @GET("movie/upcoming")
    Call<MovieResponse> getUpcomingMovie(
            @Query("language") String language,
            @Query("page") int pageCount,
            @Query("region") String region
    );

    /**
     * https://api.themoviedb.org/3/
     * movie/
     * now_playing
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &page=1
     * &region=tw
     */
    @GET("movie/now_playing")
    Call<MovieResponse> getNowPlayingMovie(
            @Query("language") String language,
            @Query("page") int pageCount,
            @Query("region") String region
    );

    /**
     * https://api.themoviedb.org/3/
     * movie/
     * top_rated
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &page=1
     * &region=tw
     */
    @GET("movie/top_rated")
    Call<MovieResponse> getTopRatedMovie(
            @Query("language") String language,
            @Query("page") int pageCount,
            @Query("region") String region
    );

    /**
     * https://api.themoviedb.org/3/
     * movie/
     * popular
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &page=1
     * &region=tw
     */
    @GET("movie/popular")
    Call<MovieResponse> getPopularMovie(
            @Query("language") String language,
            @Query("page") int pageCount,
            @Query("region") String region
    );

    /**
     * https://api.themoviedb.org/3/
     * movie/
     * 287947/
     * similar
     * ?api_key=45e912d1c05f37dc908ab2b93f9f3d42
     * &language=en-US
     * &page=1
     */
    @GET("movie/{movieId}/similar")
    Call<MovieResponse> getSimilarMovie(
            @Path("movieId")int movieId,
            @Query("language") String language,
            @Query("page") int pageCount
    );

    @GET("movie/{id}/credits")
    Call<Credits> getCredits(@Path("id") int id);

    @GET("person/{person_id}")
    Call<Actor> getActorDetails(@Path("person_id") int id);

}
