package com.cauliflower.danielt.retrofitsample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;
import android.util.Log;
import com.cauliflower.danielt.retrofitsample.RetrofitSampleApp;
import com.cauliflower.danielt.retrofitsample.network.TmdbApiService;
import com.cauliflower.danielt.retrofitsample.tmdb.BriefMovie;
import com.cauliflower.danielt.retrofitsample.tmdb.DetailedMovie;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;

import javax.inject.Inject;
import java.util.List;

public class MainViewModel extends AndroidViewModel {
    private static final String TAG = MainViewModel.class.getSimpleName();
    @Inject
    TmdbApiService tmdbApiService;

    private MutableLiveData<DetailedMovie> mSearchedMovie = new MutableLiveData<>();

    private MutableLiveData<List<BriefMovie>> mSearchedMovies = new MutableLiveData<>();
    private CompositeDisposable mCompositeDisposable;

    public MainViewModel(@NonNull Application application) {
        super(application);
        ((RetrofitSampleApp) application).getAppComponent().inject(this);
        mCompositeDisposable = new CompositeDisposable();
    }

    public void searchMovieById(final String movieId) {
        mCompositeDisposable.add(tmdbApiService.getMovieDetail(movieId)
                //The thread runs this task
                .subscribeOn(Schedulers.io())
                //In default, the result will be received in the same thread that runs the task,
                // if the result is about UI modifying, pass AndroidSchedulers.mainThread()
//                .observeOn(Schedulers.io())
                .subscribe(this::setSearchedMovie, this::logError));
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        if (mCompositeDisposable != null) {
            mCompositeDisposable.clear();
            mCompositeDisposable = null;
        }
    }

    public MutableLiveData<DetailedMovie> getSearchedMovie() {
        return mSearchedMovie;
    }

    public MutableLiveData<List<BriefMovie>> getSearchedMovies() {
        return mSearchedMovies;
    }

    private void setSearchedMovie(DetailedMovie searchedMovie) {
        this.mSearchedMovie.postValue(searchedMovie);
    }

    private void logError(Throwable t) {
        Log.e(TAG, "logError: ", t);
    }

}
