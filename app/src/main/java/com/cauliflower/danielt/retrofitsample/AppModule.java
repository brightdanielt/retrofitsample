package com.cauliflower.danielt.retrofitsample;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import dagger.Module;
import dagger.Provides;

import javax.inject.Singleton;

@Module
public class AppModule {

    private Application mApplication;

    public AppModule(Application application) {
        this.mApplication = application;
    }

    @Provides
    @Singleton
    Application getApplication() {
        return mApplication;
    }

    @Provides
    @Singleton
    Context getApplicationContext() {
        return mApplication.getApplicationContext();
    }

    @Provides
    @Singleton
    Resources getResources() {
        return mApplication.getResources();
    }
}
