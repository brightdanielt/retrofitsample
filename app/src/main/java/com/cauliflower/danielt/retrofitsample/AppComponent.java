package com.cauliflower.danielt.retrofitsample;

import com.cauliflower.danielt.retrofitsample.network.NetworkModule;
import com.cauliflower.danielt.retrofitsample.ui.MainActivity;
import com.cauliflower.danielt.retrofitsample.viewmodel.MainViewModel;
import dagger.Component;

import javax.inject.Singleton;

@Singleton
@Component(modules = {AppModule.class, NetworkModule.class})
public interface AppComponent {
    void inject(MainActivity activity);

    void inject(MainViewModel mainViewModel);
}
