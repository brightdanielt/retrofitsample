package com.cauliflower.danielt.retrofitsample.tmdb;

import java.util.List;

public class ReviewResponse {

    /**
     * id : 76341
     * page : 1
     * results : [{"author":"Phileas Fogg","content":"Fabulous action movie. Lots of interesting characters. They don't make many movies like this. The whole movie from start to finish was entertaining I'm looking forward to seeing it again. I definitely recommend seeing it.","id":"55660928c3a3687ad7001db1","url":"https://www.themoviedb.org/review/55660928c3a3687ad7001db1"},{"author":"Andres Gomez","content":"Good action movie with a decent script for the genre. The photography is really good too but, in the end, it is quite repeating itself from beginning to end and the stormy OST is exhausting.","id":"55732a53925141456e000639","url":"https://www.themoviedb.org/review/55732a53925141456e000639"},{"author":"extoix","content":"Awesome movie!  WITNESS ME will stick with me forever!","id":"55edd26792514106d600e380","url":"https://www.themoviedb.org/review/55edd26792514106d600e380"},{"author":"Cineport","content":"Fantastic action that makes up for some plot holes.","id":"5654a732c3a368176600023d","url":"https://www.themoviedb.org/review/5654a732c3a368176600023d"},{"author":"balumahendran","content":"Wonderful action movie. Scenes are unimaginable. Lots of graphics used in this movie. This is totally a very good action movie.","id":"57162d35c3a3682f450038b2","url":"https://www.themoviedb.org/review/57162d35c3a3682f450038b2"},{"author":"simonTheDiver","content":"I was never a fan of the original Mad Max films but this one just blew me away. There was something so powerfully visceral about this on the big screen. It was a fun on assault on the sense , in a good way. Its spectacle rather than plot driven so may not fare as well on the smaller screen. Watch on the largest possible tv in a darkened room with the phone and other gadgets off. Crank up the volume.","id":"5990b254925141675f0322f9","url":"https://www.themoviedb.org/review/5990b254925141675f0322f9"},{"author":"Per Gunnar Jonsson","content":"I would say that this movie is pretty much the kind of movie that I expected it to be. That being said I think it was a bit softer than I hoped it to be. I think that, again, the Hollywood plonkers have opted to make a movie that got a more palate rating (to them and their cash registers) than what it should have been. It is not as obvious as with Robocop and a few other movies but the feeling is there.\r\n\r\nIt is still a fairly entertaining movie. It is of course yet another special effects movie and as such it does not disappoint. It differs from a lot of the usual special effects movies in that the effects are pretty down to Earth in a bizarre way. We are not talking about superheroes or near magical technologies here but instead we have a devastated landscape, scrounged together equipment and a totally ludicrous desire to blow up the last remains of human civilisation. The bizarre vehicle designs and the totally ridicules behaviour of a lot of people in this movie is alone worth watching it to be honest.\r\n\r\nAs with the previous Mad Max movies this one stretches the bounds of reality (I hope) when it comes to human behaviour and how they waste the few precious resources, specifically gasoline and bullets, that remains after the apocalypse. It provides some spectacular scenery but it also puts a bit of \u201cwhat the f\u2026\u201d feeling to it all. Not that this part is not expected from a Mad Max movie though.\r\n\r\nWhat I think was my largest let-down in the movie was the Mad Max character itself. To me he is supposed to be a real bad-ass. In this movie he was not really that impressive. He certainly was far from the original Mad Max character in my opinion and was really overshadowed by the Furiosa character. I could have lived with that if this character would have been really impressive but she was not. She was cool but she was still underwhelming when comparing to anything in the original bad-ass Mad Max universe. I would say that Tina Turner and Mel Gibson still rules!\r\n\r\nStill it is a decent futuristic, apocalyptic action movie, maybe not living up to this old movie geeks expectations, but not at all a wasted movie evening. The sound track of the Blu-ray certainly gave my subwoofer an exercise as well.","id":"5a64efea0e0a2619f802ac7c","url":"https://www.themoviedb.org/review/5a64efea0e0a2619f802ac7c"}]
     * total_pages : 1
     * total_results : 7
     */

    private int id;
    private int page;
    private int total_pages;
    private int total_results;
    private List<Review> results;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPage() {
        return page;
    }

    public void setPage(int page) {
        this.page = page;
    }

    public int getTotal_pages() {
        return total_pages;
    }

    public void setTotal_pages(int total_pages) {
        this.total_pages = total_pages;
    }

    public int getTotal_results() {
        return total_results;
    }

    public void setTotal_results(int total_results) {
        this.total_results = total_results;
    }

    public List<Review> getResults() {
        return results;
    }

    public void setResults(List<Review> results) {
        this.results = results;
    }


}
