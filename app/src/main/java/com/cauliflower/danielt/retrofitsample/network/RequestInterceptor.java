package com.cauliflower.danielt.retrofitsample.network;

import com.cauliflower.danielt.retrofitsample.BuildConfig;
import okhttp3.HttpUrl;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

import javax.inject.Inject;
import java.io.IOException;

public class RequestInterceptor implements Interceptor {

    @Inject
    RequestInterceptor() {
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
//        Request original = chain.request();
//        HttpUrl originalHttpUrl = original.url();
//
//        HttpUrl url = originalHttpUrl.newBuilder()
//                .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
//                .build();
//
//        Request request = original.newBuilder().url(url).build();
//        return chain.proceed(request);

        Request originalRequest = chain.request();
        HttpUrl originalHttpUrl = originalRequest.url();
        HttpUrl url = originalHttpUrl.newBuilder()
                .addQueryParameter("api_key", BuildConfig.TMDB_API_KEY)
                .build();
        Request request = originalRequest.newBuilder().url(url).build();
        return chain.proceed(request);
    }
}
