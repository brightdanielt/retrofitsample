# RetrofitSample

First experience of Retrofit2 + RxJava2 + Dagger2 with ViewModel(Android Architecture Component)

This app tries to get info from [TMDB API](https://developers.themoviedb.org/3/getting-started/introduction)

Reference: (Medium is a great place!)


**Post:**
*  [Android MVVM with Dagger 2, Retrofit, RxJava, Architecture Components](https://android.jlelse.eu/android-mvvm-with-dagger-2-retrofit-rxjava-architecture-components-6f5da1a75135)
    
*  [Implementing RxJava2 & Retrofit2 for better performance during API calls](https://android.jlelse.eu/implementing-rxjava2-retrofit2-for-better-performance-during-api-calls-fe1c53e1f939)
    
*  [RXAndroid + Retrofit](https://medium.com/mindorks/rxandroid-retrofit-2fff4f89fa85)

*  [Dagger 2 for Android Beginners](https://medium.com/@harivigneshjayapalan/dagger-2-for-android-beginners-introduction-be6580cb3edb)
    (Because of this post, I watch Game Of Thrones haha)
    
**Project**
*  [MovieGuide](https://github.com/esoxjem/MovieGuide)
    
*  [For more project](https://blog.aritraroy.in/20-awesome-open-source-android-apps-to-boost-your-development-skills-b62832cf0fa4)
    